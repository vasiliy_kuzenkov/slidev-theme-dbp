# slidev-theme-dbp

[![Netlify Status](https://api.netlify.com/api/v1/badges/6ef0b0de-61f9-488a-aa21-a6b800108c85/deploy-status)](https://app.netlify.com/sites/strong-youtiao-de4c2e/deploys) [![NPM version](https://img.shields.io/npm/v/slidev-theme-dualboot?color=005AF0&label=slidev-theme-dualboot)](https://www.npmjs.com/package/slidev-theme-vuetiful)

Dualboot Partners presentation theme for [Slidev](https://github.com/slidevjs/slidev).

[Live demo](https://dbp-slides.bondiano.io/) | [Documentation](docs/index.md) | [Example](example.md) | [Contribution Guide](CONTRIBUTING.md)

## Usage

### As Slidev theme

Add the following frontmatter to your `slides.md`. Start Slidev then it will prompt you to install the theme automatically.

<pre><code>---
theme: <b>dualboot</b>
---</code></pre>

Learn more about [how to use a theme](https://sli.dev/themes/use).

## Layouts

This theme provides the following layouts:

### Intro `intro`

Usage:

```
---
layout: intro
author: bondiano (Vassiliy Kuzenkov)
---
```

![Slide demo with intro page](/docs/assets/screenshots/intro.jpeg)

### Title slide `title`

Usage:

```
---
layout: title
---
```

![Slide demo with title page](/docs/assets/screenshots/title.jpeg)

### Default `default`

![Slide demo with defaults page](/docs/assets/screenshots/default.jpeg)

### Quote or major idea slide `quote`

Usage:

```
---
layout: quote
author: Vassiliy
---
```

![Slide demo with quote page](/docs/assets/screenshots/quote.jpeg)

### Centred layout `center`

Usage:
```
---
layout: center
---
```

![Slide demo with quote page](/docs/assets/screenshots/center.jpeg)

### Nice slide with code and terminal `code-terminal`

Usage:
```
---
layout: code-terminal
---

<code-block>

::terminal::

<terminal-block>

```

![Slide demo with quote page](/docs/assets/screenshots/code_with_terminal.jpeg)

### To show something as window `text-window`

Usage:

```
---
layout: text-window
reverse: true/false
---

<text-block>

::window::

<embed-window-block>

```

![Slide demo with quote page](/docs/assets/screenshots/text_window.jpeg)

## Local development

### Install dependencies

```bash
yarn
```

### Start dev server

```bash
yarn dev
```
