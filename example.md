---
theme: ./
favicon: /favicon.png
layout: intro
author: bondiano (Vassiliy Kuzenkov)
---

_Example Subtitle_

# Example title

_Example description_

---

# Slide Title

Slide Subtitle

* Slide bullet text
  * Slide bullet text
  * Slide bullet text
* Slide bullet text
* Slide bullet text

---
layout: center
---

# Slide Title

Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

---
layout: quote
author: Vassiliy
---

# Big quotes make your talk look fancy

---

# Code

```ts {all|2|1-6|all}
interface User {
  id: number
  firstName: string
  lastName: string
  role: string
}

function updateUser(id: number, update: Partial<User>) {
  const user = getUser(id)
  const newUser = {...user, ...update}
  saveUser(id, newUser)
}
```

---
layout: code-terminal
---

# Code with terminal

```ts {all}
// user.ts
const user = { name: 'John', age: 30, role: 'user' }
const admin = { name: 'Administrator', age: 25, role: 'admin' }
console.log(user)
console.log(admin)
console.log('final')
```

::terminal::

<console-input>
  node user.ts
</console-input>
<console-output>
  { name: 'John', age: 30, role: 'user' }
</console-output>
<console-output>
  { name: 'Administrator', age: 25, role: 'admin' }
</console-output>
<console-output>
  final
</console-output>

---
layout: title
---

_Example Subtitle_

# Title Slide

---
layout: text-window
reverse: true
---

# Embedded stuff

Use window to show a live demo of any page, or even a sub component!

::window::

<div class="overflow-hidden relative w-full aspect-16-9">
  <iframe height="300" style="width: 100%;" scrolling="no" title="Text Clock" src="https://codepen.io/searleb/embed/pvQaJB?default-tab=html%2Cresult" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
    See the Pen <a href="https://codepen.io/searleb/pen/pvQaJB">
    Text Clock</a> by Bill Searle (<a href="https://codepen.io/searleb">@searleb</a>)
    on <a href="https://codepen.io">CodePen</a>.
  </iframe>
</div>

---
layout: column
cols: 1-2
---

```vue
<template>
  <h1>Hello World"</h1>
  <div class="message">{{ message }}</div>
</template>
<script>
  export default {
    data: () => ({
      message: 'Great to be here!',
    })
  }
</script>
<style scoped>
  .message {
    color: red;
  }
</style>
```

::right::

# Features

* Flexible column width via [unoCSS](https://unocss.dev/)
* Here, more weight on the left
* Leaves room for short notes

---
layout: center
class: "text-center"
---


# Learn More

[Documentations](https://sli.dev) / [GitHub Repo](https://github.com/slidevjs/slidev)
