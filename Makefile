setup:
	yarn

start:
	yarn dev

publish:
	npx release-it

.PHONY: setup start publish
