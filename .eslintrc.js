const OFF = 'off'
const ERROR = 'error'
const WARN = 'warn'

module.exports = {
  root: true,

  env: {
    browser: true,
    node: true,
    jest: true,
  },

  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },

  extends: [
    'plugin:vue/vue3-recommended',
    '@vue/prettier',
    '@vue/typescript',
    'prettier',
  ],
  rules: {
    'prettier/prettier': ERROR,
  },

  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'max-len': OFF,
        'vue/multi-word-component-names': OFF,
      },
    },
  ],
}
