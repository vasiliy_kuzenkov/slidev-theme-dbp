import {
  defineConfig,
  presetAttributify,
  presetTypography,
  presetWebFonts,
  presetUno,
  presetWind,
  presetIcons,
} from 'unocss'

import transformerDirectives from '@unocss/transformer-directives'

export default defineConfig({
  transformers: [transformerDirectives()],
  presets: [
    presetAttributify(),
    presetWebFonts(),
    presetUno(),
    presetTypography(),
    presetWind(),
    presetIcons(),
  ],
  theme: {
    colors: {
      primary: '#ffffff',
      brand: {
        primary: '#ffffff',
        black: '#000000',
        accent: '#4285f4',
        link: '#0097a7',
        gray: {
          primary: '#bcbdbf',
          secondary: '#595959',
        },
      },
    },
  },
})
