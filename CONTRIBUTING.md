# Contributing

- `yarn`
- `yarn dev` to start theme preview of `example.md`
- Edit the `example.md` and style to see the changes
- `yarn export` to generate the preview PDF
- `yarn screenshot` to generate the preview PNG
