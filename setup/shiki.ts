import { defineShikiSetup } from '@slidev/types'

export default defineShikiSetup(() => ({
  theme: {
    dark: 'github-dark',
    light: 'github-dark',
  },
}))
